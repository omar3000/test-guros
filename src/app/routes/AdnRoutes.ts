import express from "express";
import { AdnController } from "../../app/controllers/AdnController";
import { AdnService } from "../../domain/services/AdnService";
import { AdnRepository } from "../../domain/repositories/AdnRepository";
import { SequelizeAdnRepository } from "../../infrastructure/persistence/sequelize/repositories/SequelizeAdnRepository";
import { chekAuth }  from "../../infrastructure/jwt/JwtService";

const router = express.Router();
const adnRepository: AdnRepository = new SequelizeAdnRepository();
const adnService: AdnService = new AdnService(adnRepository);
const adnController: AdnController = new AdnController(adnService);

router.post(
  "/mutation",
  chekAuth,
  adnController.createValidationRules(),
  adnController.createAdn.bind(adnController)
);

router.get(
  "/stats",
  chekAuth,
  adnController.getAdnStats.bind(adnController)
);

export default router;
