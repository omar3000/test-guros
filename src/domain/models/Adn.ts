export interface Adn {
    id: string;
    userid: string;
    adn: string;
    isMutation: boolean;
    createdAt: Date;
    updatedAt: Date;
}