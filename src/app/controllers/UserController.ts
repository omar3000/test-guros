import { Request, Response } from "express";
import { UserService } from "../../domain/services/UserService";
import { CreateUserDTO } from "../../app/dtos/CreateUserDTO";
import { Validation } from "../../infrastructure/validation/Validation";
import { body } from "express-validator";

export class UserController {
  private userService: UserService;

  constructor(userService: UserService) {
    this.userService = userService;
  }

  public loginValidationRules() {
    return [
      body("email").isEmail().withMessage("Email is invalid"),
      body("password").notEmpty().withMessage("Email is invalid"),
    ];
  }

  public createValidationRules() {
    return [
      body("email").isEmail().withMessage("Email is invalid"),
      body("password").notEmpty().withMessage("Password is neccesary"),
      body("name").notEmpty().withMessage("Name is neccesary"),
    ];
  }


  async createUser(req: Request, res: Response) {
    try {

      const errors  = Validation.validate(req);

      if (errors) {
        res.status(422).send({errors: errors});
        return;
      }
      
      const createUserDTO: CreateUserDTO = req.body;
      const user = await this.userService.createUser(createUserDTO);

      res.status(user.statusCode).json(user.body ?? user.error);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error});
    }
  }

  async loginUser(req: Request, res: Response) {
    try {
      const errors  = Validation.validate(req);

      if (errors) {
        res.status(422).send({errors: errors});
        return;
      }
      const createUserDTO: CreateUserDTO = req.body;
      const user = await this.userService.loginUser(createUserDTO);

      res.status(user.statusCode).json(user.body || user.error);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error});
    }
  }

}