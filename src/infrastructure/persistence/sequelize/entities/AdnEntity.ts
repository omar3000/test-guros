import { Model, DataTypes } from "sequelize";
import sequelize from "../../../config/database";
import { UserEntity } from "./UserEntity";

export class AdnEntity extends Model {
  public id!: string;
  public adn!: string;
  public isMutation!: boolean; 
  public createdAt!: Date;
  public updatedAt!: Date;
}

AdnEntity.init(
  {
    id: {
      primaryKey: true,
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },
    userid: {
      type: DataTypes.STRING,
      references: { model: "users", key: "id" },
      allowNull: false,
    },
    adn: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    isMutation: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false,
    },
  },
  {
    sequelize,
    tableName: "adn",
  }
);

UserEntity.belongsTo(AdnEntity, {foreignKey: "id"});

AdnEntity.hasOne(UserEntity, {foreignKey: "id",sourceKey: "userid"});