import { Adn } from "../../../../domain/models/Adn";
import { AdnRepository } from "../../../../domain/repositories/AdnRepository";
import { AdnEntity } from "../entities/AdnEntity";
export class SequelizeAdnRepository implements AdnRepository {
  async createAdn(adn): Promise<Adn> {
    const createdAdn = await AdnEntity.create(adn);
    return createdAdn.toJSON() as Adn;
  }

  async findByAdn(adn: string): Promise<Adn | null> {
    const findAdn = await AdnEntity.findOne({where: { adn } });

    if (!findAdn) {
      return null;
    }
    return findAdn.toJSON() as Adn;
  }

  async countMutations(isMutation: boolean): Promise<number> {
    const countAdn = await AdnEntity.count({ where: { isMutation} });

    if (!countAdn) {
      return 0;
    }
    return countAdn;
  }

  async countNoMutations(): Promise<number> {
    const countAdn = await AdnEntity.count();

    if (!countAdn) {
      return 0;
    }
    return countAdn;
  }
}