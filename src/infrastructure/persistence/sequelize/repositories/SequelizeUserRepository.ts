import { User } from "../../../../domain/models/User";
import { UserRepository } from "../../../../domain/repositories/UserRepository";
import { UserEntity } from "../entities/UserEntity";

export class SequelizeUserRepository implements UserRepository {
  async createUser(user): Promise<User> {
    const createdUser = await UserEntity.create(user);
    return createdUser.toJSON() as User;
  }

  async findByEmail(email: string): Promise<User | null> {
    console.log(email);
    const user = await UserEntity.findOne({where: { email } });

    if (!user) {
      return null;
    }
    return user.toJSON() as User;
  }
}