import express from "express";
import { UserController } from "../../app/controllers/UserController";
import { UserService } from "../../domain/services/UserService";
import { UserRepository } from "../../domain/repositories/UserRepository";
import { SequelizeUserRepository } from "../../infrastructure/persistence/sequelize/repositories/SequelizeUserRepository";

const router = express.Router();
const userRepository: UserRepository = new SequelizeUserRepository();
const userService: UserService = new UserService(userRepository);
const userController: UserController = new UserController(userService);

router.post(
  "/",
  userController.createValidationRules(),
  userController.createUser.bind(userController)
);

router.post(
  "/login",
  userController.loginValidationRules(),
  userController.loginUser.bind(userController)
);

export default router;
