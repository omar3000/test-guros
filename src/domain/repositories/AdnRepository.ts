import { Adn } from "../models/Adn";

export interface AdnRepository {
  createAdn(adn: Adn): Promise<Adn>;
  findByAdn(adn: string): Promise<Adn | null>;
  countMutations(ismutation: boolean): Promise<number>;
  countNoMutations(): Promise<number>;
}