# api-adn

Api desarrollada con ts, nodejs, express, jwt, postgresql,swagger,sequelize, jest <br>
Se utilizo una arquitectura hexagonal para la estructura de la api utiliza
patrónes de diseño como inversión de control, inyeccion de dependencias y la separacion
de responsabilidades.

## Configuracion

Renombrar el archivo `.env-example ` por `.env ` en la raiz <br>
Cambiar la información del archivo con tu informacion local <br>

Ejecutar el comando para instalar todas las librerias del proyecto <br>

```
npm install -all
```
Puedes usar la imagen de postgrsql y de pgadmin que viene en el archivo docker-compose
Asi no tendras que instalar el motor en tu maquina local solo ejecuta

```
docker-compose up
```

Ejecutar el comando para migrar las tablas en la bd local. <br>
(Cabe recalcar que antes de ejecutar el comando crear la base de datos vacia) <br>

```
sequelize db:migrate
```
# Ejecucion

Ejecutar el proyecto en modo dev <br>

```
npm run dev
```

Abrir en su navegador
`http://localhost:3000/api-docs`

Te aparecerá una ventana de swagger con todos los endpoints <br>
1. Deberas crear un usuario con el metodo post de user<br>
2. Despues hacer login con las credenciales del usuario creado <br>
3. El token que genere la respuesta de login copiarlo y pegarlo <b>sin comillas</b> en el candado de cualquier endpoint para que puedas acceder a los otros dos.

###### Nota: si no metes el jwt, los endpoints de `adn/mutation` y `adn/stats` devolveran un 401 auth failed ya que tienen seguridad y solo usuarios validados pueden acceder

## Tests

Para las pruebas unitarias en el `userId` y `email` para generar el token, poner un usuario valido de su base de datos <br>
en el archivo. `___tests___/index.test.js` <br>

Para ejecutar los tests con jest <br>

```
npm run test
```

## Deploy en AWS:

Primero transpilamos a js y guardamos el transpilado en dist con el siguiente comando:
```
npm run build
```
Despues desplegamos para esto debimos ya configurar nuestras credenciales en `serverless credentials'

```
serverless deploy
```
Despues como hicimos en local podemos ejecutar las migraciones en la base de datos de rds que creamos.

La url publica con swagger es:
https://r6qzxkgt4nnxrb5i7i65ldlyju0wshly.lambda-url.us-east-1.on.aws/api-docs/ 

Aqui aplican las mismas indicaciones que las mencionadas en la parte de arriba en local para swagger.

## Complejidad del algoritmo

Para optimizar el algortimo `hasMutation(dna: string[])` si el dna existe en la bd consulta el resultado y ya no lo calcula
para no hacer calculos innecesarios. 

El algoritmo tiene promesas en cada validacion por fila, columna y diagonales.
En cuanto se encuentra la mutacion se detiene la ejecucion y retorna el resultado. 

En el peor caso la complejidad es `O(n^3)`, pero con la ejecucion paralela de las promesas y los registros ya guardados otpmizamos un poco el prceso. 
