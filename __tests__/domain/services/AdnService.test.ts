import { AdnService } from "../../../src/domain/services/AdnService";
import { AdnRepositoryMock } from "../repositories/AdnRepositoryMock.test";
import { CreateAdnDTO } from "../../../src/app/dtos/CreateAdnDTO";

describe("AdnService", () => {
  let adnService: AdnService;
  let adnRepositoryMock: AdnRepositoryMock;

  beforeEach(() => {
    adnRepositoryMock = new AdnRepositoryMock();
    adnService = new AdnService(adnRepositoryMock);
  });

  describe("createAdn", () => {
    it("should return 200 if adn already exists in the database and has mutation", async () => {
      const createAdnDTO: CreateAdnDTO = {
        dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"],
        userid: "testuserid",
      };

      const result = await adnService.createAdn(createAdnDTO);

      expect(result.statusCode).toEqual(200);
    });

    it("should return 403 if adn does not  exist in the database and does not have mutation", async () => {
      const createAdnDTO: CreateAdnDTO = {
        dna: ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"],
        userid: "testuserid",
      };

      const result = await adnService.createAdn(createAdnDTO);

      expect(result.statusCode).toEqual(403);
    });

    it("should create new adn and return 200 if adn already exists in the database and has mutation", async () => {
      const createAdnDTO: CreateAdnDTO = {
        dna: ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"],
        userid: "testuserid",
      };

      const result = await adnService.createAdn(createAdnDTO);

      expect(result.statusCode).toEqual(200);
    });
  });

  describe("getadnstats", () => {

    it("Get Adn Stats", async () => {
  
      const result = await adnService.getAdnStats();
  
      expect(result.statusCode).toEqual(200);
      expect(result.body).toEqual({ "count_mutations": 5, "count_no_mutations": 10, "ratio": "0.50"});

    });
  });
});