import { User } from "../models/User";
import { HttpResponse } from "../../app/interfaces/HttpResponse";
import { UserRepository } from "../repositories/UserRepository";
import { CreateUserDTO } from "../../app/dtos/CreateUserDTO";
import { LoginUserDTO } from "../../app/dtos/LoginUserDTO";
import {v4 as uuidv4} from "uuid";
import bcrypt  from "bcryptjs";
import  jwt  from "jsonwebtoken"; 

export class UserService {
  private userRepository: UserRepository;
  private saltRounds: number;

  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
    this.saltRounds = 10;

  }

  async createUser(createUserDTO: CreateUserDTO): Promise<HttpResponse> {
    // Validar y transformar los datos según sea necesario
    // ...
    const existingUser = await this.userRepository.findByEmail(createUserDTO.email);
    if (existingUser) {
      return { statusCode: 409, error: "Email duplicated"};
    }

    const salt = await bcrypt.genSalt(this.saltRounds);

    const hashedPassword = await bcrypt.hash(createUserDTO.password, salt);

    const user: User = {
      id: uuidv4(),
      name: createUserDTO.name,
      email: createUserDTO.email,
      password: hashedPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    return {statusCode: 201, body: await this.userRepository.createUser(user) };
  }

  async loginUser(LoginUserDTO: LoginUserDTO): Promise<HttpResponse> {

    const existingUser = await this.userRepository.findByEmail(LoginUserDTO.email);

    if(!existingUser){
      return { statusCode: 404, error: "User not exists"};
    }

    const result = await bcrypt.compare(LoginUserDTO.password, String(existingUser?.password));

    if (!result){
      return { statusCode: 401, error: "invalid credentials"};
    }
      
    const token = jwt.sign(
      {
        email: existingUser?.email,
        userId: existingUser?.id,
        name: existingUser?.name
      },
      String(process.env.JWT_KEY),
      {
        expiresIn: "500h"
      },
    );

    return { statusCode: 200, body: token};

  }

}