import { Adn } from "../models/Adn";
import { HttpResponse } from "../../app/interfaces/HttpResponse";
import { AdnRepository } from "../repositories/AdnRepository";
import { CreateAdnDTO } from "../../app/dtos/CreateAdnDTO";
import {v4 as uuidv4} from "uuid";

export class AdnService {
  private adnRepository: AdnRepository;

  constructor(adnRepository: AdnRepository) {
    this.adnRepository = adnRepository;
  }

  async createAdn(createAdnDTO: CreateAdnDTO): Promise<HttpResponse> {

    const stringDna = createAdnDTO.dna.join(",");
    const existingAdn = await this.adnRepository.findByAdn(stringDna);

    //If it exists in the db, it is no longer necessary 
    //to check the mutation because the result is in the db
    //and validation only one adn for register no repeats
    if(existingAdn){
      return {statusCode: existingAdn.isMutation? 200 : 403};
    }

    const adn: Adn = {
      id: uuidv4(),
      userid: createAdnDTO.userid,
      adn: stringDna,
      isMutation: await this.hasMutation(createAdnDTO.dna),
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await this.adnRepository.createAdn(adn);

    return {statusCode: adn.isMutation? 200 : 403 };
  }

  async getAdnStats(): Promise<HttpResponse> {

    const adnMutations = await this.adnRepository.countMutations(true);
    const totalMutations = await this.adnRepository.countNoMutations();
    let ratio = "0";
    if (totalMutations !== 0)
      ratio = (adnMutations / totalMutations).toFixed(2);

    return {statusCode: 200, body: { "count_mutations": adnMutations, "count_no_mutations": totalMutations, ratio }};
  }

  private async hasMutation(dna: string[]): Promise<boolean> {

    const promises: Promise<boolean>[] = [];
    const n = dna.length;
    const targetRegex = /(A{4}|T{4}|C{4}|G{4})/;

    // Validation rows
    promises.push(
      new Promise( (resolve) => {
        for (let i = 0; i < n; i++) {
          if (targetRegex.test(dna[i])) {
            resolve(true);
            return;
          }
        }
        resolve(false);
      })
    );
  
    // Validations columns
    promises.push(
      new Promise((resolve) => {
        for (let j = 0; j < n; j++) {
          let column = "";
          for (let i = 0; i < n; i++) {
            column += dna[i][j];
          }
          if (targetRegex.test(column)) {
            resolve(true);
            return;
          }
        }
        resolve(false);
      })
    );
  
    // Validataion diagonals
    promises.push(
      new Promise((resolve) => {
        for (let i = 0; i < n; i++) {
          for (let j = 0; j < n; j++) {
            let diagonal = "";
            for (let k = 0; k < n - Math.max(i, j); k++) {
              diagonal += dna[i + k][j + k];
            }
            if (targetRegex.test(diagonal)) {
              resolve(true);
              return;
            }
          }
        }
        resolve(false);
      })
    );
    return await Promise.race(promises);
  }

}


