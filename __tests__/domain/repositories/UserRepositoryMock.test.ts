import { UserRepository } from "../../../src/domain/repositories/UserRepository";
import { User } from "../../../src/domain/models/User";
import bcrypt  from "bcryptjs";

export class UserRepositoryMock implements UserRepository {
  
  private saltRounds:number;

  constructor(){
    this.saltRounds = 10;
  }


  async findByEmail(email: string): Promise<User | null> {

    const salt = await bcrypt.genSalt(this.saltRounds);

    const hashedPassword = await bcrypt.hash("12345", salt);

    const existingUser: User = {
      id: "iduser",
      name: "nameuser",
      email: "email@hotmail.com",
      password: hashedPassword,
      createdAt: new Date(),
      updatedAt: new Date()
    };

    return email === existingUser.email ? existingUser : null;
  }

  async createUser(user: User): Promise<User> {
    return user;
  }
}