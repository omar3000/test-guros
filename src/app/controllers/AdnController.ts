import { Response } from "express";
import { AdnService } from "../../domain/services/AdnService";
import { CreateAdnDTO } from "../../app/dtos/CreateAdnDTO";
import { Validation } from "../../infrastructure/validation/Validation";
import { body } from "express-validator";

export class AdnController {
  private adnService: AdnService;

  constructor(adnService: AdnService) {
    this.adnService = adnService;
  }

  public createValidationRules() {
    return [
      body("dna").isArray().withMessage("La propiedad debe ser un array").custom((value) => {
        const regex = /^[ATCG]+$/;

        for (const element of value) {
          if (typeof element !== "string" || !regex.test(element)) {
            throw new Error("Los elementos del array deben ser (A, T, C, G)");
          }
        }
      
        return true;
      })

    ];
  }


  async createAdn(req, res: Response) {
    try {

      const errors  = Validation.validate(req);

      if (errors) {
        res.status(422).send({errors: errors});
        return;
      }
      
      const createAdnDTO: CreateAdnDTO = {
        userid: req.userData.userId,
        dna: req.body?.dna
      };
      const adn = await this.adnService.createAdn(createAdnDTO);

      res.status(adn.statusCode).json(adn.body ?? adn.error);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error});
    }
  }

  async getAdnStats(req, res: Response) {
    try {
      
      const adn = await this.adnService.getAdnStats();
      res.status(adn.statusCode).json(adn.body ?? adn.error);
    } catch (error) {
      console.log(error);
      res.status(500).json({ error: error});
    }
  }


}