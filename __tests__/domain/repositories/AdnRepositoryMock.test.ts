import { AdnRepository } from "../../../src/domain/repositories/AdnRepository";
import { Adn } from "../../../src/domain/models/Adn";

// Mock de AdnRepository
export class AdnRepositoryMock implements AdnRepository {
  async findByAdn(adn: string): Promise<Adn | null> {
    const existingAdn:Adn = 
    { 
      id: "testid",
      userid: "testuserid",
      adn: "ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG", 
      isMutation: true,
      createdAt: new Date(), 
      updatedAt: new Date()
    };
    return adn === existingAdn.adn ? existingAdn : null;
  }

  async createAdn(adn: Adn): Promise<Adn> {
    return adn;
  }

  async countMutations(isMutation: boolean): Promise<number> {
    return 5;
  }

  async countNoMutations(): Promise<number> {
    return 10;
  }
}