import request from "supertest";
import { appTest } from "../src/app/index";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config();

describe("Unit tests", () => {

  const token = jwt.sign({
    email: "emaildb",
    userId: "useriddb"
  },
  process.env.JWT_KEY);

  it("returns has mutation", async () => {
    const nums = [
      {"input": { "dna": ["ATGCGA", "CAGTGC", "TTATTT", "AGACGG", "GCGTCA", "TCACTG"] }, "status": 403},
      {"input": { "dna": ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"] }, "status": 200},
      {"input": { "dna": ["ATGCGA", "CAGTGC", "TTTTGT", "AGAAGG", "CCCCTA", "TCCCCG"] }, "status": 200},
      {"input": { "dna": ["ATTTTA", "CGGAGC", "AATTTT", "AGACGG", "GTTATA", "TCACTG"] }, "status": 200},
    ];

    for (let i = 0; i < nums.length; i++) {
      const num = nums[i];
      const response = await request(appTest).post("/adn/mutation").set("Authorization", `${token}`).send(num.input);
      expect(response.status).toBe(num.status);
    }
  });

});