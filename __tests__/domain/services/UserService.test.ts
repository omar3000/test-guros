import { UserService } from "../../../src/domain/services/UserService";
import { UserRepositoryMock } from "../repositories/UserRepositoryMock.test";
import { CreateUserDTO } from "../../../src/app/dtos/CreateUserDTO";
import { LoginUserDTO } from "../../../src/app/dtos/LoginUserDTO";

describe("UserService", () => {
  let userService: UserService;
  let userRepositoryMock: UserRepositoryMock;

  beforeEach(() => {
    userRepositoryMock = new UserRepositoryMock();
    userService = new UserService(userRepositoryMock);
  });

  describe("createUser", () => {
    it("should return 201 if user created in database", async () => {
      const createUserDTO: CreateUserDTO = {
        name: "username",
        email: "email2@hotmail.com",
        password: "12345"
      };

      const result = await userService.createUser(createUserDTO);

      expect(result.statusCode).toEqual(201);
    });

    it("should return 409 if email already exists in the database", async () => {
        
      const createUserDTO: CreateUserDTO = {
        name: "username",
        email: "email@hotmail.com",
        password: "12345"
      };

      const result = await userService.createUser(createUserDTO);

      expect(result.statusCode).toEqual(409);
      expect(result.error).toEqual("Email duplicated");
    });
  });

  describe("Login", () => {

    it("should return 401 if password incorrect", async () => {

      const loginUserDTO: LoginUserDTO = {

        email: "email@hotmail.com",
        password: "incorrect password"
      };
  
      const result = await userService.loginUser(loginUserDTO);
  
      expect(result.statusCode).toEqual(401);
      expect(result.error).toEqual("invalid credentials");

    });

    it("should return 404 if email does not exists in database", async () => {

      const loginUserDTO: LoginUserDTO = {
        email: "noexist@hotmail.com",
        password: "12345"
      };
    
      const result = await userService.loginUser(loginUserDTO);
    
      expect(result.statusCode).toEqual(404);
      expect(result.error).toEqual("User not exists");
  
    });

    it("should return 200 if email and password is correct in database", async () => {

      const loginUserDTO: LoginUserDTO = {
        email: "email@hotmail.com",
        password: "12345"
      };
      
      const result = await userService.loginUser(loginUserDTO);
      
      expect(result.statusCode).toEqual(200);
    
    });


  });
});